from PIL import Image, ImageDraw, ImageFont
from urllib.parse import urlparse
from urllib.request import urlopen
from urllib.parse import unquote

import requests

from io import BytesIO


def produce_module_image(url: str, module_number: int) -> None:
    print("Initializing template..")
    canvas = Image.open("./assets/template.png")
    operator_name = unquote(urlparse(url).query.split("=")[1])
    lookup_table = requests.get(CHARACTER_TABLE)
    print("Inferring character info..")
    for code, data in lookup_table.json().items():
        if data["appellation"] == operator_name:
            character_code = code.split("_")[2]
            character_subclass = data["subProfessionId"]
            character_rarity = data["rarity"] + 1
    print("Grabbing assets..")
    module_code_string = MODULE_CODE.format(module_number + 1, character_code)
    module_icon_url = MODULE_ICON_URL.format(module_code_string)
    module_icon = grab_asset(module_icon_url)
    module_type_string = requests.get(MODULE_TYPES).json()["equipDict"][module_code_string]["typeIcon"]
    module_type_url = MODULE_TYPE_URL.format(module_type_string)
    module_type = grab_asset(module_type_url)
    subclass_icon = grab_asset(SUBCLASS_ICON_URL.format(character_subclass))
    print("Stitching assets..")
    canvas = stitch_assets(canvas, module_icon, module_type, subclass_icon, character_rarity)
    print("Saving..")
    canvas.save(f"./cards/{operator_name}_{module_number}.png")
    print("Done.")
    input("Press Enter to close: ")


def grab_asset(url: str) -> Image:
    return Image.open(urlopen(url))


def stitch_assets(canvas: Image, module_icon: Image, module_type: Image, subclass_icon: Image, rarity: int) -> Image:
    module_icon_x = 0
    module_icon_y = (ICON_SPACE_HEIGHT - IMAGE_WIDTH) // 2
    module_icon = module_icon.resize((IMAGE_WIDTH, IMAGE_WIDTH), resample=4)
    canvas.paste(module_icon, (module_icon_x, module_icon_y))
    module_type = module_type.resize((MODULE_ICON_SIZE, MODULE_ICON_SIZE))
    subclass_icon = subclass_icon.resize((MODULE_ICON_SIZE, MODULE_ICON_SIZE))
    canvas.paste(subclass_icon, (10, INFO_SPACE_START))
    canvas.paste(module_type, (10, INFO_SPACE_START + INFO_SPACE_HEIGHT // 2))
    text_image = Image.new("RGBA", (500, 500))
    drawer = ImageDraw.Draw(text_image)
    font = ImageFont.truetype("./assets/myriad-bold-italic.ttf", 185)
    drawer.text((0, 0), str(rarity * 10), font=font, fill=RARITY_COLOR_TABLE[rarity])
    canvas.paste(text_image.crop(text_image.getbbox()), LEVEL_STAT_START)
    drawer.rectangle((0, 0, 500, 500), (0, 0, 0, 0))
    font = ImageFont.truetype("./assets/myriad-bold-italic.ttf", 210)
    drawer.text((0, 0), str(f"{rarity}"), font=font, fill=RARITY_COLOR_TABLE[rarity])
    canvas.paste(text_image.crop(text_image.getbbox()), RARITY_STAT_START)
    drawer.rectangle((0, 0, 500, 500), (0, 0, 0, 0))
    font = ImageFont.truetype("./assets/star.ttf", 230)
    drawer.text((0, 0), u"\u2605", font=font, fill=RARITY_COLOR_TABLE[rarity])
    text_image = text_image.crop(text_image.getbbox()).rotate(-17.5, expand=True)
    text_image = text_image.crop(text_image.getbbox())
    canvas.paste(text_image, STAR_SYMBOL_START)
    return canvas


IMAGE_WIDTH = 635
IMAGE_HEIGHT = 1350

ICON_SPACE_HEIGHT = 799
INFO_SPACE_HEIGHT = 462
INFO_SPACE_START = 888
LEVEL_STAT_START = (433, 1164)
RARITY_STAT_START = (350, 938)
STAR_SYMBOL_START = (473, 948)
MODULE_ICON_SIZE = 215


CHARACTER_TABLE = "https://aceship.github.io/AN-EN-Tags/json/gamedata/zh_CN/gamedata/excel/character_table.json"
MODULE_ICON_URL = "https://raw.githubusercontent.com/Aceship/Arknight-Images/main/equip/icon/{}.png"
MODULE_CODE = "uniequip_00{}_{}"
MODULE_TYPES = "https://aceship.github.io/AN-EN-Tags/json/gamedata/zh_CN/gamedata/excel/uniequip_table.json"
MODULE_TYPE_URL = "https://raw.githubusercontent.com/Aceship/Arknight-Images/main/equip/type/{}.png"
SUBCLASS_ICON_URL = "https://raw.githubusercontent.com/Aceship/Arknight-Images/main/ui/subclass/sub_{}_icon.png"


RARITY_COLOR_TABLE = {
    4: (201, 160, 220), #C9A0DC
    5: (255, 195, 0),   #FFC300
    6: (255, 123, 0)    #FF7B00
}

if __name__ == "__main__":
    url = input("Aceship page link: ")
    module = int(input("Module number: "))
    produce_module_image(url, module)